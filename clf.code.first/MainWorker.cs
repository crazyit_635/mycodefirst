﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace clf.code.first
{
    public class MainWorker
    {
        private static IHandller handller;

        public static void SqliteRun(Assembly assembly, string ConnectionStr, Action<ProgressResult> callback)
        {
            try
            {
                handller = new SqliteHandle(ConnectionStr);
                handller?.StartHandle(assembly, callback);
            }
            catch (Exception ex)
            {
                throw ex;
            }                
        }

        public static void MysqlRun(Assembly assembly,string server,string database,string userID,string password,int port, Action<ProgressResult> callback)
        {
            try
            {
                handller =new MysqlHandle(server,database,userID,password,port);
                handller?.StartHandle(assembly, callback);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
