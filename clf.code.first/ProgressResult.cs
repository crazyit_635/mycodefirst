﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clf.code.first
{
    public class ProgressResult
    {
        public string Message { get; set; } = "";
        public int Progess { get; set; } = 0;
    }
}
