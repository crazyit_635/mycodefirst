﻿using System;
using System.Collections.Generic;
using System.Text;

namespace clf.code.first
{
    [AttributeUsage(AttributeTargets.All, Inherited = true)]
    public class BaseAttribute : Attribute
    {

        /// <summary>
        /// 目录数据库存类型 Default DatabasesType.Sqlite;
        /// </summary>
        public DatabasesType TargetDbType { get; set; } = DatabasesType.Sqlite;
    }
}
