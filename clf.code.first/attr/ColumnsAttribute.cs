﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clf.code.first
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false)]
    public class ColumnsAttribute : BaseAttribute
    {
        /// <summary>
        /// 字段名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 是否可空，默认 True
        /// </summary>
        public bool IsNull { get; set; } = true;

        public int Length { get; set; }
        /// <summary>
        /// 小数位数 默认2
        /// </summary>
        public byte Digits { get; set; } = 2;
        public object DefaultValue { get; set; } =null;

        /// <summary>
        /// 注释
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        ///  当TargetDbType =   DatabasesType.Mysql  时采用 ，默认  MySqlDbType.VarChar;
        /// </summary>
        public MySqlDbType DbType { get; set; } = MySqlDbType.VarChar;

        /// <summary>
        ///  当TargetDbType =   DatabasesType.Sqlite  时采用， 默认   SqliteDbType.Text;
        /// </summary>
        public SqliteDbType SqliteDbType { get; set; } = SqliteDbType.Text;
    }
}
