﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clf.code.first
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false)]
    public class IdAttribute : BaseAttribute
    {
        public bool AutoIncrement { get; set; } = false;


        /// <summary>
        /// Mysql bigint ,Sqlite Integer
        /// </summary>
        public MySqlDbType DbType { get; set; } = MySqlDbType.Int64;

    }
}
