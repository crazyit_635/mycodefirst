﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clf.code.first
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class TableAttribute : BaseAttribute
    {
        /// <summary>
        /// 表名
        /// </summary>

        public string Name { get; set; }

        /// <summary>
        /// 注释
        /// </summary>

        public string Comment { get; set; }


    }
}
