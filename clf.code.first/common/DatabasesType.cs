﻿using System;
using System.Collections.Generic;
using System.Text;

namespace clf.code.first
{
    /// <summary>
    /// 数据库的类型
    /// </summary>
    public enum DatabasesType
    {
        Sqlite,
        Mysql
    }
}
