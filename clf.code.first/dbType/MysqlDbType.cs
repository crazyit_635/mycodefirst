﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clf.code.first
{
    //
    // 摘要:
    //     Specifies MySQL specific data type of a field, property, for use in a MySql.Data.MySqlClient.MySqlParameter.
    public enum MySqlDbType
    {
        //
        // 摘要:
        //     MySql.Data.MySqlClient.MySqlDbType.Decimal
        //     A fixed precision and scale numeric value between -1038 -1 and 10 38 -1.
        Decimal = 0,
        //
        // 摘要:
        //     MySql.Data.MySqlClient.MySqlDbType.Byte
        //     The signed range is -128 to 127. The unsigned range is 0 to 255.
        Byte = 1,
        //
        // 摘要:
        //     MySql.Data.MySqlClient.MySqlDbType.Int16
        //     A 16-bit signed integer. The signed range is -32768 to 32767. The unsigned range
        //     is 0 to 65535
        Int16 = 2,
        //
        // 摘要:
        //     Specifies a 24 (3 byte) signed or unsigned value.
        Int24 = 9,
        //
        // 摘要:
        //     MySql.Data.MySqlClient.MySqlDbType.Int32
        //     A 32-bit signed integer
        Int32 = 3,
        //
        // 摘要:
        //     MySql.Data.MySqlClient.MySqlDbType.Int64
        //     A 64-bit signed integer.
        Int64 = 8,
        //
        // 摘要:
        //     System.Single
        //     A small (single-precision) floating-point number. Allowable values are -3.402823466E+38
        //     to -1.175494351E-38, 0, and 1.175494351E-38 to 3.402823466E+38.
        Float = 4,
        //
        // 摘要:
        //     MySql.Data.MySqlClient.MySqlDbType.Double
        //     A normal-size (double-precision) floating-point number. Allowable values are
        //     -1.7976931348623157E+308 to -2.2250738585072014E-308, 0, and 2.2250738585072014E-308
        //     to 1.7976931348623157E+308.
        Double = 5,
        //
        // 摘要:
        //     A timestamp. The range is '1970-01-01 00:00:00' to sometime in the year 2037
        Timestamp = 7,
        //
        // 摘要:
        //     Date The supported range is '1000-01-01' to '9999-12-31'.
        Date = 10,
        //
        // 摘要:
        //     Time
        //     The range is '-838:59:59' to '838:59:59'.
        Time = 11,
        //
        // 摘要:
        //     DateTime The supported range is '1000-01-01 00:00:00' to '9999-12-31 23:59:59'.
        DateTime = 12,
       
       
        //
        // 摘要:
        //     A year in 2- or 4-digit format (default is 4-digit). The allowable values are
        //     1901 to 2155, 0000 in the 4-digit year format, and 1970-2069 if you use the 2-digit
        //     format (70-69).
        Year = 13,
        //
        // 摘要:
        //     Obsolete Use Datetime or Date type
        Newdate = 14,
        //
        // 摘要:
        //     A variable-length string containing 0 to 65535 characters
        VarString = 0xF,
        //
        // 摘要:
        //     Bit-field data type
        Bit = 0x10,
        //
        // 摘要:
        //     JSON
        JSON = 245,
        //
        // 摘要:
        //     New Decimal
        NewDecimal = 246,
        //
        // 摘要:
        //     An enumeration. A string object that can have only one value, chosen from the
        //     list of values 'value1', 'value2', ..., NULL or the special "" error value. An
        //     ENUM can have a maximum of 65535 distinct values
        Enum = 247,
        //
        // 摘要:
        //     A set. A string object that can have zero or more values, each of which must
        //     be chosen from the list of values 'value1', 'value2', ... A SET can have a maximum
        //     of 64 members.
        Set = 248,
        //
        // 摘要:
        //     A binary column with a maximum length of 255 (2^8 - 1) characters
        TinyBlob = 249,
        //
        // 摘要:
        //     A binary column with a maximum length of 16777215 (2^24 - 1) bytes.
        MediumBlob = 250,
        //
        // 摘要:
        //     A binary column with a maximum length of 4294967295 or 4G (2^32 - 1) bytes.
        LongBlob = 251,
        //
        // 摘要:
        //     A binary column with a maximum length of 65535 (2^16 - 1) bytes.
        Blob = 252,
        //
        // 摘要:
        //     A variable-length string containing 0 to 255 bytes.
        VarChar = 253,
        //
        // 摘要:
        //     A fixed-length string.
        String = 254,
        //
        // 摘要:
        //     Geometric (GIS) data type.
        Geometry = 0xFF,
        //
        // 摘要:
        //     Unsigned 8-bit value.
        UByte = 501,
        //
        // 摘要:
        //     Unsigned 16-bit value.
        UInt16 = 502,
        //
        // 摘要:
        //     Unsigned 24-bit value.
        UInt24 = 509,
        //
        // 摘要:
        //     Unsigned 32-bit value.
        UInt32 = 503,
        //
        // 摘要:
        //     Unsigned 64-bit value.
        UInt64 = 508,
        //
        // 摘要:
        //     Fixed length binary string.
        Binary = 754,
        //
        // 摘要:
        //     Variable length binary string.
        VarBinary = 753,
        //
        // 摘要:
        //     A text column with a maximum length of 255 (2^8 - 1) characters.
        TinyText = 749,
        //
        // 摘要:
        //     A text column with a maximum length of 16777215 (2^24 - 1) characters.
        MediumText = 750,
        //
        // 摘要:
        //     A text column with a maximum length of 4294967295 or 4G (2^32 - 1) characters.
        LongText = 751,
        //
        // 摘要:
        //     A text column with a maximum length of 65535 (2^16 - 1) characters.
        Text = 752,
        //
        // 摘要:
        //     A guid column.
        Guid = 854
    }
}

