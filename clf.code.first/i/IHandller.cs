﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SqlDao;

namespace clf.code.first
{
    public interface IHandller
    {
        void StartHandle(Assembly assembly,Action<ProgressResult> callback);
    }
}
