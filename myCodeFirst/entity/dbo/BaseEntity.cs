﻿using clf.code.first;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myCodeFirst
{
    /// <summary>
    /// 基类   有默认值的类型字段 是好是加？ 转换成Nullable 可空类型，防止修改出错。
    /// </summary>
    public class BaseEntity
    {
        [Id(AutoIncrement = false)]
        [Columns(DbType = MySqlDbType.Int64 ,SqliteDbType =SqliteDbType.Integer,Length =30,IsNull =false,Comment ="主键")]
        public Int64 id;
        public Int64 Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
    }
}
