using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using clf.code.first;
using System.Data;
using System.Xml.Linq;
using System.Windows.Documents;

namespace myCodeFirst
{
    [Table(Comment ="车辆信息")]
    public class CarInfo : BaseEntity
    {
        [Columns(DbType = MySqlDbType.VarChar, Length = 30, IsNull = true, Comment = "车辆号")]
        public String carNumber;

        [Columns(DbType = MySqlDbType.Decimal,SqliteDbType =SqliteDbType.Real, Length = 10,Digits =3, IsNull =false ,DefaultValue =0.00, Comment = "皮重")]
        public Decimal? traeWeight;

        [Columns(DbType = MySqlDbType.VarChar, Length = 20,  IsNull = true, DefaultValue ="", Comment = "驾驶员")]
        public String driver;

        [Columns(DbType = MySqlDbType.VarChar, Length = 13, IsNull = true, DefaultValue = "", Comment = "联系电话")]
        public String driverMobile;

        [Columns(DbType = MySqlDbType.VarChar, Length = 30, IsNull = true, DefaultValue = "身份证号")]
        public String driverIdnumber;

        [Columns(DbType = MySqlDbType.Int32, Length = 4, IsNull = true)]
        public Int64? ownerId;

        [Columns(DbType = MySqlDbType.VarChar, Length = 30, IsNull = true,Comment = "车主名称")]
        public String ownerName;

        [Columns(DbType = MySqlDbType.Int16,SqliteDbType =SqliteDbType.Integer, Length = 2, IsNull = true ,DefaultValue =0,Comment = "是否已经有IC卡(0:否,1:是)")]
        public Boolean? hasIc;

        [Columns( Length = 30, IsNull = true,DefaultValue ="",Comment = "IC 编号")]
        public String icNumber;
        [Columns(DbType = MySqlDbType.Int16,SqliteDbType =SqliteDbType.Blob, IsNull = false, DefaultValue = 0, Comment = "是否删除(0:否,1:是)")]
        public Boolean? idDelete;
        [Columns(DbType = MySqlDbType.DateTime, IsNull = true, DefaultValue = null, Comment = "删除时间")]
        public DateTime? deleteTime;
        [Columns(DbType = MySqlDbType.Int64,SqliteDbType =SqliteDbType.Integer, IsNull = false, DefaultValue = 0, Comment = "同步时间")]
        public Int64? syncTime;

    }
}
