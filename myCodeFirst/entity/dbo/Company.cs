using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using clf.code.first;

namespace myCodeFirst
{
    [Table(Comment ="共客 公司信息")]
    public class Company : BaseEntity
    {
        [Columns(DbType = MySqlDbType.VarChar,Length =50,IsNull =true,DefaultValue =null,Comment ="全称")]
        public String fullName;

        [Columns(DbType = MySqlDbType.VarChar, Length = 50, IsNull = true, DefaultValue = null, Comment = "简称")]
        public String shortName;

        [Columns(DbType = MySqlDbType.VarChar, Length = 50, IsNull = true, DefaultValue = null, Comment = "首拼")]
        public String nameFirstCase;

        [Columns(DbType = MySqlDbType.Int16, Length = 2, IsNull = false, DefaultValue = 0, Comment = "是否为煤矿 1是 0 否")]
        public Boolean? isCoalSource;

        [Columns(DbType = MySqlDbType.Int16, Length = 2, IsNull = false, DefaultValue = 0, Comment = "是否在线，使用系统")]
        public Boolean? isOnline;

        [Columns(DbType = MySqlDbType.VarChar, Length = 50, IsNull = true, DefaultValue = null, Comment = "法人")]
        public String legalPerson;

        [Columns(DbType = MySqlDbType.VarChar, Length = 50, IsNull = true, DefaultValue = null, Comment = "联系人")]
        public String connectPeople;

        [Columns(DbType = MySqlDbType.VarChar, Length = 50, IsNull = true, DefaultValue = null, Comment = "联系电话")]
        public String connectPhone;
        [Columns(DbType = MySqlDbType.DateTime,  DefaultValue = null)]
        public DateTime? addtime;

    }
}
