using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using clf.code.first;

namespace myCodeFirst
{
    [Table(Comment = "物料信息")]
    public class Material : BaseEntity
    {
        [Columns(DbType = MySqlDbType.VarChar, Length = 50, IsNull = true, Comment = "名称")]
        public String name;

        [Columns(DbType = MySqlDbType.Int64, IsNull = true)]
        public Int64? categoryId;

        [Columns(DbType = MySqlDbType.DateTime, IsNull = true, Comment = "添加时间")]
        public DateTime? addtime;

        [Columns(DbType = MySqlDbType.Int64, IsNull = true, DefaultValue = null, Comment = "添加时间")]
        public Int64? addUserId;

        [Columns(DbType = MySqlDbType.VarChar, Length = 100, IsNull = true, DefaultValue = null, Comment = "添加人")]
        public String addUserName;

        [Columns(DbType = MySqlDbType.DateTime, IsNull = true, DefaultValue = null, Comment = "更新时间")]
        public DateTime? updateTime;

        [Columns(DbType = MySqlDbType.Int32, Length = 100, IsNull = true, DefaultValue = 0, Comment = "状态 (1: 正常, 0:禁用)")]
        public Int32? status;

        [Columns(DbType = MySqlDbType.Decimal, Length = 10, Digits = 3, IsNull = true, DefaultValue = 0.00, Comment = "")]
        public Decimal stone;

        [Columns(DbType = MySqlDbType.Decimal, Length = 10, Digits = 3, IsNull = true, DefaultValue = 1, Comment = "采购总数")]
        public Decimal totalBuy;

        [Columns(DbType = MySqlDbType.Decimal, Length = 10, Digits = 3, IsNull = true, DefaultValue = 1, Comment = "销售总数")]
        public Decimal totalSale;

        [Columns(DbType = MySqlDbType.Double, Length = 10, Digits = 3, IsNull = true, DefaultValue = 0.00, Comment = "成本价")]
        public Double buyPrice;

        [Columns(DbType = MySqlDbType.Float, Length = 10, Digits = 3, IsNull = true, DefaultValue = 0.00, Comment = "售价")]
        public float saleMoney;

    }
}
