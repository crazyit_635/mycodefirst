﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using clf.code.first;

namespace myCodeFirst
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Title = FindResource("test") as String;

            UpdateSqlite();

            UpdateMySql();

        }

        /// <summary>
        /// 更新 Sqlite 数据库结构
        /// </summary>
        private void UpdateSqlite()
        {
            string connstring = string.Empty;
            Assembly assembly = Assembly.GetExecutingAssembly();
            IEnumerable<TypeInfo> definedTypes = assembly.DefinedTypes;
            MainWorker.SqliteRun(assembly, connstring, CallBack);
        }

        /// <summary>
        /// 更新 Mysq 数据库结构
        /// </summary>
        private void UpdateMySql()
        {
            string Server = "127.0.0.1";
            string DataBase = "my_code";
            string UserId = "root";
            string Password = "local@clf123456";
            int Port = 3306;
            Assembly assembly = Assembly.GetExecutingAssembly();
            IEnumerable<TypeInfo> definedTypes = assembly.DefinedTypes;
            MainWorker.MysqlRun(assembly, Server, DataBase, UserId, Password, Port, CallBack);
        }

        private void CallBack(ProgressResult result)
        {
            this.Dispatcher.Invoke(() =>
            {
                this.infoLb.Items.Add(result.Message + " Progess:" + result.Progess);
            });
        }
    }
}
