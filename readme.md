# code.first.mysql

​ 代码到数据库存的工具

### 要求

1. 代码对业务工程，无侵入。
2. 以类的字段为主，**数据库字段在类字段没有的一律删除**
3. 只有 Table 注解标注的类才表示数据库中有表
4. 没有 Columns 注解标记的数据表中不保存

### 始策略

​ 开发模式：每一次启动（生成）处理

​ 生主环境：通过配制版本号(更新或重装，重置)后处理

### 运行时机

​ 宿主程序，主动调用;异步 运行 并报告进度信息

1. 如果在 Debug 模式下，启动一次运行一次

   ```
    /// <summary>
    /// 更新 Sqlite 数据库结构
    /// </summary>
    private void UpdateSqlite()
    {
        string connstring = string.Empty;
        Assembly assembly = Assembly.GetExecutingAssembly();
        IEnumerable<TypeInfo> definedTypes = assembly.DefinedTypes;
        MainWorker.SqliteRun(assembly, connstring, CallBack);
    }

    /// <summary>
    /// 更新 Mysq 数据库结构
    /// </summary>
    private void UpdateMySql()
    {
        string Server = "127.0.0.1";
        string DataBase = "my_code";
        string UserId = "root";
        string Password = "local@clf123456";
        int Port = 3306;
        Assembly assembly = Assembly.GetExecutingAssembly();
        IEnumerable<TypeInfo> definedTypes = assembly.DefinedTypes;
        MainWorker.MysqlRun(assembly, Server, DataBase, UserId, Password, Port, CallBack);
    }
   ```

   2. 在 RELEASE 模式下
      1. 程序更新时，运行
      2. 配制数据库，储存时运行

​

```

```

### 处理规则

#### 表

​ 1.新加入的类，增加表
​ 2.后加入的类，更新表
​ 3.数据表中多余 Table， 不处理

#### 列

​ 1.新加入的类 Field，增加 Column
​ 2.后加入的类 Field，更新表 Column
​ 3.数据表中多余 Colum 删除

### Attributes 特性 同理 Annotation 注解

1. Table

   1. Name
   2. Comment

2. Id

   1. AutoIncrement
   2. Type

3. Columns

   1.Name

   2. Type
   3. Length
   4. IsNull
   5. Default
   6. Comment

### 类型对应表

| C# class Type | Len | Mysql DB Type    | Len |
| ------------- | --- | ---------------- | --- |
| bool          |     | tinyint          |     |
| Int32         |     | int integer      |     |
| int64         |     | bigint           |     |
| Decmail       |     | decmail          |     |
| Float Double  |     | double           |     |
| String        |     | varchar text     |     |
|               |     | 其余的基本用不到 |     |

### 注意

1. 主键 只会第一次构建时创建，一般是（id），后期修改主键无效。

### 优势

1. 深度学习，得到提升
2. 插件式项目，功能插件，可以自动生成数据库
   1. 可以不在同一台服务器
   2. 可以不是同一个数据库
   3. 插件的增加和删除，不影响原来的宿主项目

### 单独的 代码仓库管理

[Git](https://gitee.com/crazyit_635/mycodefirst)
